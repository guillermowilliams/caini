import React, { Component } from 'react'
import { Layout, Menu, Avatar, Dropdown, Icon, Badge } from 'antd';
import { Link, NavLink } from "react-router-dom"

import Logo from "./../assets/images/logo.png"

const { Header, Content, Footer } = Layout;

const menuUser = (props) =>(
    <Menu>
      <Menu.Item>
        <b>{localStorage.getItem('name') + ' ' + localStorage.getItem('lastname')}</b>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">Perfil</a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">Configuración</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3" onClick={() => props.fnLogout()}>
        Cerrar sesión
      </Menu.Item>
    </Menu>
);

export default class MyLayout extends Component {
    constructor(props){
        super(props);
        if (!localStorage.getItem('email')) {
            window.location.href="/"
        }
        this.state = {
            nameShort : localStorage.getItem('name').substr(0,1).toUpperCase()
        }
    }
    componentWillMount = () =>{
        document.title = "Caini - "+this.props.title   
    }
    fnLogout = () =>{
        window.location.href="/"
        localStorage.removeItem('username');
        localStorage.removeItem('email');
        localStorage.removeItem('lastname');
        localStorage.removeItem('name');
    }
    render() {
            return (
                <div id="components-layout-demo-top">
                <Layout className="layout">
                    <Header>
                        <div className="logo">
                            <Link to={"/dashboard"}><div className="logo-image" style={{backgroundImage:`url(${Logo})`}} /></Link>
                        </div>
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            style={{ lineHeight: '64px' }}
                        >
                            <Menu.Item>
                                <NavLink to={"/dashboard"} activeClassName="active"><Icon type="dashboard" />Inicio</NavLink>
                            </Menu.Item>
                            <Menu.Item>
                                <NavLink to={"/personal"} activeClassName="active"><Icon type="team" />Personal</NavLink>
                            </Menu.Item>
    
                            <div className="ddAvatar">
                                <Dropdown overlay={ () => menuUser(this)}>
                                    <div className="ant-dropdown-link" href="#">
                                    <Badge dot={true}>
                                    <Avatar style={{ backgroundColor: '#87d068', verticalAlign: 'middle' }} size="large">
                                        <b>{this.state.nameShort}</b>
                                    </Avatar>
                                    </Badge>
                                    &nbsp;&nbsp; <Icon type="down" />
                                    </div>
                                </Dropdown>
                            </div>
                        </Menu>
                        
                    </Header>
                    <Content style={{ padding: '0 50px' }}>
                        {this.props.children}
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>
                        Desarrollo por Guillermo ©2019
                    </Footer>
                </Layout>
                </div>
            )
    }
}
