import React, { Component } from 'react'
import { Row, Col } from 'antd';
import Formulario from "./../components/login/Formulario"

export default class Login extends Component {
    componentWillMount = () =>{
        if (localStorage.getItem("email")) {
            this.props.history.push('/dashboard')
        }
    }
    render() {  
        return (
            <div className="section-login">
            <Row style={{width:'100%'}}>
                <Col sm={{ span:12, offset:0 }} md={{ span:12, offset:6 }}>
                    <div id="components-form-demo-normal-login">
                        <Formulario MyRoute={this}/>
                    </div>
                </Col>
            </Row>
            </div>
        )
    }
}


