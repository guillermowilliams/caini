import React, { Component } from 'react'
import { Breadcrumb } from 'antd';
import MyLayout from "./../layout/MyLayout"

export default class Dashboard extends Component {
    render() {
        return (
            <MyLayout title="Inicio">
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item>List</Breadcrumb.Item>
                    <Breadcrumb.Item>App</Breadcrumb.Item>
                </Breadcrumb>
                <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>Content</div>
            </MyLayout>
        )
    }
}
