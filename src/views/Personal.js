import React, { Component } from 'react'
import { Breadcrumb } from 'antd';
import MyLayout from "./../layout/MyLayout"
import InicioTable from "./../components/personal/InicioTable"

export default class Personal extends Component {
    render() {
        return (
            <MyLayout title="Inicio">
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Inicio</Breadcrumb.Item>
                    <Breadcrumb.Item>Personal</Breadcrumb.Item>
                </Breadcrumb>
                <div style={{ background: '#fff', padding: 24 }}>
                    <InicioTable />
                </div>
            </MyLayout>
        )
    }
}
