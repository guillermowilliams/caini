import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { BackTop } from 'antd';

import ScrollTop from "./../components/others/ScrollToTop"
import Page404 from "./../components/others/Page404"
import Login from "./../views/Login"
import Dashboard from "./../views/Dashboard"
import Personal from "./../views/Personal"

export default class MyRouter extends Component {
    render() {
        return (
            <Router>
                <ScrollTop>
                <div className="App">
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <Route exact path="/dashboard" component={Dashboard} />
                        <Route exact path="/personal" component={Personal} />
                        <Route component={Page404} />
                    </Switch>
                </div>
                <BackTop />
                </ScrollTop>
            </Router>
        )
    }
}
