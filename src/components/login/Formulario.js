import React, { Component } from 'react'
import axios from "axios"
import {
    Form, Icon, Input, Button, Checkbox, Card, notification
} from 'antd';
import passwordHash from "password-hash"
import Logo from "./../../assets/images/logo.png"

const openNotificationWithIcon = type => {
    notification[type]({
      message: 'Notification Title',
      description:
        'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    });
};

class Formulario extends Component {
    constructor(props){
        super(props)
        this.state = {
            loading: false,
            iconLoading: false,
        }
    }

    enterLoading = () => {
        this.setState({ loading: true });
    }
    
    enterIconLoading = () => {
        this.setState({ iconLoading: true });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        var thes = this;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                axios.get('https://sente.jjuica.com/Controller/Json.php', {
                    params: {
                        action:'MyUser',
                        page:'MyUser',
                        method:'get_login',
                        p_username: values.username
                    }
                })
                .then(function (response) {
                    if(passwordHash.verify(values.password, response.data.usu_password)){
                        localStorage.setItem("name", response.data.name);
                        localStorage.setItem("lastname", response.data.lastname);
                        localStorage.setItem("username", response.data.username);
                        localStorage.setItem("email", response.data.email);
                        window.location.href = "/dashboard";
                    }else{
                        notification.warning({ message: 'Inicio de sesión', description: 'Usuario o contraseña incorrectos. Intentelo nuevamente.' })
                        thes.setState({ loading: false, iconLoading: false })
                    } 
                    
                })
                .catch(function (error) {
                    thes.setState({ loading: false, iconLoading: false })
                });
            }else{
                notification.warning({ message: 'Inicio de sesión', description: 'Usuario o contraseña incorrectos. Intentelo nuevamente.' })
                this.setState({ loading: false, iconLoading: false })
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                <Card>
                    <Form.Item>
                        <div className={"LogoLogin"} style={{ backgroundImage: `url(${Logo})` }} />
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: 'Ingrese usuario!' }],
                        })(
                            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Usuario" />
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Ingrese contraseña!' }],
                        })(
                            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Contraseña" />
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(
                            <Checkbox>Recordar</Checkbox>
                        )}
                        <a className="login-form-forgot" href="">Recuperar contraseña</a>
                        <Button type="primary" htmlType="submit" className="login-form-button" loading={this.state.loading} onClick={this.enterLoading}>
                            Ingresar
                        </Button>
                    </Form.Item>
                </Card>
            </Form>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ 
    name: 'normal_login' 
})(Formulario);

export default WrappedNormalLoginForm;