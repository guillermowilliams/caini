import React, { Component } from 'react'
import { Table, Input, Button, Icon, Divider, Modal } from 'antd';
import Highlighter from 'react-highlight-words';

const data = [{
    key: '1',
    name: 'Guillermo',
    lastname: 'Juica Ruidias',
    direccion: 'Asoc. de vivienda Raucana Mz H lte 49',
    email: 'guillermojuica3@gmail.com',
    cargo: 'Desarrollo de software',
    area: 'Sistemas',
    options: ''
}];

function confirm() {
    Modal.confirm({
      title: 'Confirmar',
      content: '¿Esta seguro que desea eliminar el item?',
      okText: 'Aceptar',
      cancelText: 'Cancelar',
    });
}

export default class InicioTable extends Component {
    constructor(props){
        super(props)
        this.state = {
            searchText: '',
            modalVisible: false
        }
    }

    getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
          setSelectedKeys, selectedKeys, confirm, clearFilters,
        }) => (
          <div style={{ padding: 8 }}>
            <Input
              ref={node => { this.searchInput = node; }}
              placeholder={`Buscar ${dataIndex}`}
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Button
              type="primary"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              size="small"
              style={{ width: 90, marginRight: 8 }}
            >
              Buscar
            </Button>
            <Button
              onClick={() => this.handleReset(clearFilters)}
              size="small"
              style={{ width: 90 }}
            >
              Limpiar
            </Button>
          </div>
        ),
        filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
        render: (text) => (
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.state.searchText]}
            autoEscape
            textToHighlight={text.toString()}
          />
        ),
    })
    
    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
    }
    
    handleReset = (clearFilters) => {
        clearFilters();
        this.setState({ searchText: '' });
    }

    render() {
        const columns = [{
            title: 'Nombres',
            dataIndex: 'name',
            key: 'name',
            width: '20%',
            ...this.getColumnSearchProps('name'),
        }, {
            title: 'Apellidos',
            dataIndex: 'lastname',
            key: 'lastname',
            width: '20%',
            ...this.getColumnSearchProps('lastname'),
        }, {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            ...this.getColumnSearchProps('email'),
        }, {
            title: 'Cargo',
            dataIndex: 'cargo',
            key: 'cargo',
            ...this.getColumnSearchProps('cargo'),
        }, {
            title: 'Area',
            dataIndex: 'area',
            key: 'area',
            ...this.getColumnSearchProps('area'),
        }, {
            title: '',
            key: 'action',
            render: (text, record) => (
              <span>
                <Button type="danger" shape="circle" icon="delete" onClick={confirm}/>
                <Divider type="vertical" />
                <Button type="primary" shape="circle" icon="edit" />
              </span>
            ),
        }];
        return (
            <div>
                <b><Icon type="user" style={{fontSize:24}}/> Mantenimiento de personal</b>
                <Divider />
                <Button type="primary" shape="round" icon="plus" style={{float:'right'}}>Nuevo personal</Button>
                <br /><br />
                <Table columns={columns} dataSource={data} />
            </div>
        )
    }
}
