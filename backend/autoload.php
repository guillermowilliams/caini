<?php  

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

class Autoload{
	public function __construct(){
		spl_autoload_register(array($this, 'loader'));
	}
	public function loader($class = array()){
		$exceptions = [
			'Header'=>'theme',
			'Footer'=>'theme'
		];

		if (array_key_exists($class, $exceptions)) {
			$filename = dirname(__FILE__).DIRECTORY_SEPARATOR.$exceptions[$class].'/'.$class.'.php';	
		}else{
			if (strpos($class,'View') === false) {
				$filename = dirname(__FILE__).DIRECTORY_SEPARATOR.'Controller/'.$class.'.php';
			}else{
				$filename = dirname(__FILE__).DIRECTORY_SEPARATOR.'Views/'.$class.'.php';
			}
		}
		if (file_exists($filename)) {
	        require_once($filename);
	    }
	}
}
$Autoload = new Autoload();