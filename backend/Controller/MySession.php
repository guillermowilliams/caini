<?php

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

class MySession{
	function __construct(){
		if(!session_id()){
			session_start();
		}
	}
	public function get($pp1){
		return isset($_SESSION[$pp1]) ? $_SESSION[$pp1] : false;
	}
	public function set($pp1, $pp2){
		$_SESSION[$pp1] = $pp2;
	}
	public function delete($pp1){
		unset($_SESSION[$pp1]);
	}
	public function destroy(){
		session_unset();
		session_destroy();
		setcookie (session_name(), '', time()-300, '/', '', 0);
	}
}