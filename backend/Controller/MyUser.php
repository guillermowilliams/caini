<?php
require_once(dirname(__FILE__) . '/MyPDO.php');
class MyUser extends MyPDO{
	public function get_login($params = array()){
		$prepare = $this->prepare('call sp_get_login(:p_username)');
		$prepare->bindValue(':p_username', $params['p_username']);
		$prepare->execute();
		$cached = $prepare->fetch(PDO::FETCH_ASSOC);
		return $cached;
	}
}
