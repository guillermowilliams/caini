<?php
require_once('../autoload.php');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

$ll2 = ucfirst($_REQUEST['action']);
$method = $_REQUEST['method'];
$action = new $ll2();
echo json_encode($action->$method($_REQUEST));