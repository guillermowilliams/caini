<?php  

require_once(dirname(__FILE__) . '/MyPDO.php');

class MySlider extends MyPDO
{
	
	public function get_slider($params=array()){
		$prepare = $this->prepare('call sp_get_slider()');
		$prepare->execute();
		$cached = $prepare->fetchAll(PDO::FETCH_ASSOC);
		return $cached;
    }
    public function crud_delete_slider($params = array()){
        $prepare = $this->prepare('CALL sp_crud_delete_slider(:p_id)');
	    $prepare->bindvalue(':p_id',$params['p_id']);
	    $prepare->execute();
	    $cached = $prepare->fetch(PDO::FETCH_ASSOC);
		return $cached;
    }
    public function crud_create_slider($params = array()){
        $values = $this->prepare('CALL sp_crud_create_slider(:p_title, :p_subtitle, :p_description, :p_image, :p_id_especialidad)');
	    $values->bindValue(':p_title', $params['p_title']);
		$values->bindValue(':p_subtitle', $params['p_subtitle']);
		$values->bindValue(':p_description', $params['p_description']);
		$values->bindValue(':p_image', $params['p_image']);
		$values->bindValue(':p_id_especialidad', $params['p_id_especialidad']);
	    $values->execute();

	    $cached = $values->fetch(PDO::FETCH_ASSOC);
	    return $cached;
    }
    public function crud_update_slider($params = array()){
        $values = $this->prepare('CALL sp_crud_update_slider(:p_id, :p_title, :p_subtitle, :p_description, :p_image, :p_id_especialidad)');
        $values->bindValue(':p_id', $params['p_id']);
        $values->bindValue(':p_title', $params['p_title']);
		$values->bindValue(':p_subtitle', $params['p_subtitle']);
		$values->bindValue(':p_description', $params['p_description']);
		$values->bindValue(':p_image', $params['p_image']);
		$values->bindValue(':p_id_especialidad', $params['p_id_especialidad']);
	    $values->execute();

	    $cached = $values->fetch(PDO::FETCH_ASSOC);
	    return $cached;
    }
}

?>